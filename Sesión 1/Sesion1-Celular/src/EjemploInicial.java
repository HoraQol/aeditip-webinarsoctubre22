import java.time.LocalDateTime;

public class EjemploInicial {

	public static void main(String[] args) {
		int varEntera;
		double varPF;
		String varString;
		LocalDateTime varTiempo;
		
		Celular unCelular = new Celular();
		//unCelular.proveedor = "Huawei"; // ERROR
		unCelular.setProveedor("Huawei");
		//System.out.println("Proveedor: " + unCelular.proveedor);	// ERROR
		System.out.println("Proveedor: " + unCelular.getProveedor());
		Celular otroCelular = new Celular("Huawei", "Y7", 2,
				13.5, true);
		otroCelular.imprimir();
	}

}
