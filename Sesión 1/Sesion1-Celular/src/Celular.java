
public class Celular {
	// Atributos
	private String proveedor;
	private String marca;
	private int numCamaras;
	private double nivelBateria;
	private boolean huellaDactilar;
	
	// Métodos
	// Constructor
	public Celular() {
	}
	
	public Celular(String proveedor, String marca) {
		this.proveedor = proveedor;
		this.marca = marca;
	}
	
	public Celular(int camaras, double bateria) {
		this.numCamaras = camaras;
		this.nivelBateria = bateria;
	}
	
	public Celular(String proveedor, String marca, int numCamaras,
			double nivelBateria, boolean huellaDactilar) {
		this.proveedor = proveedor;
		this.marca = marca;
		this.numCamaras = numCamaras;
		this.nivelBateria = nivelBateria;
		this.huellaDactilar = huellaDactilar;
	}
	
	
	// Método de clase
	/*public String imprimir() {
		return "El celular " + proveedor + "-" + marca +
				" tiene " + numCamaras + " cámaras, con una batería " +
				"que dura " + nivelBateria + " horas y que" +
				(huellaDactilar ? "": " no") +
				" tiene detector de huella dactilar.";
	}*/
	
	public void imprimir() {
		System.out.println("El celular " + proveedor + "-" + marca +
				" tiene " + numCamaras + " cámaras, con una batería " +
				"que dura " + nivelBateria + " horas y que" +
				(huellaDactilar ? "": " no") +
				" tiene detector de huella dactilar.");
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getNumCamaras() {
		return numCamaras;
	}

	public void setNumCamaras(int numCamaras) {
		this.numCamaras = numCamaras;
	}

	public double getNivelBateria() {
		return nivelBateria;
	}

	public void setNivelBateria(double nivelBateria) {
		this.nivelBateria = nivelBateria;
	}

	public boolean isHuellaDactilar() {
		return huellaDactilar;
	}

	public void setHuellaDactilar(boolean huellaDactilar) {
		this.huellaDactilar = huellaDactilar;
	}
}
