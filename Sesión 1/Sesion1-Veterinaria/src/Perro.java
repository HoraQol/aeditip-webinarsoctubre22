// Superclase: Animal; Subclase: Perro
public class Perro extends Animal {
	public double intensidadLadridodB;
	
	public Perro(String nombre, int edad, String colorPelo,
			String raza, double intensidadLadridodB) {
		// Llamar al constructor de la superclase.
		super(nombre, edad, colorPelo, raza);
		// Asigna valor al atributo de la subclase
		this.intensidadLadridodB = intensidadLadridodB;
		this.setEspecie(1);
	}
	
	public String toString() {
		return this.getNombre() + ", de " +
				this.getEdad() + " años, de color " +
				this.getColorPelo().toLowerCase()
				+ " y raza " + this.getRaza() + 
				" tiene una intensidad de ladrido de " +
				this.intensidadLadridodB + " dB.";
	}
	
	//@Override // <- Sobrecarga de m�todos
	public int calcularEdad() {
		return 7*this.getEdad();
	}
}
