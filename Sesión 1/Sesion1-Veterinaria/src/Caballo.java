
public class Caballo extends Animal {
	private double velocidad;
	public Caballo(String nombre, int edad, String colorPelo,
			String raza, double velocidad) {
		super(nombre, edad, colorPelo, raza);
		this.velocidad = velocidad;
		this.setEspecie(3);
	}
	
	public String toString() {
		return this.getNombre() + ", de " +
				this.getEdad() + " años, de color " +
				this.getColorPelo().toLowerCase()
				+ " y raza " + this.getRaza() + 
				" tiene una velocidad de " +
				this.velocidad + " m/s.";
	}
	
	// Edad de caballo: 15 + 2.5*años
	public int calcularEdad() {
		return 15 + (int)(2.5*this.getEdad());
	}
}
