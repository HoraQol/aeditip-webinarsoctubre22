
public class Gato extends Animal {
	private double alturaMaximaSalto;
	
	public Gato(String nombre, int edad, String colorPelo,
			String raza, double alturaMaximaSalto) {
		super(nombre, edad, colorPelo, raza);
		this.alturaMaximaSalto = alturaMaximaSalto;
		this.setEspecie(2);
	}
	
	public String toString() {
		return this.getNombre() + ", de " +
				this.getEdad() + " años, de color " +
				this.getColorPelo().toLowerCase()
				+ " y raza " + this.getRaza() + 
				" tiene una altura máxima de salto de " +
				this.alturaMaximaSalto + " m.";
	}
	
	public int calcularEdad() {
		// variable = (comparaci�n ? valorSiV : valorSiF )
		/* Equivale a:
		 * if(comparaci�n){
		 * 		variable = valorSiV;
		 * else{
		 * 		variable = valorSiF;
		 * }
		 */
		return (this.getEdad()>0 ? 15: 0) +
			   (this.getEdad()>1 ? 10: 0) +
			   (this.getEdad()>2 ? 4*(this.getEdad()-2): 0);
	}
}
