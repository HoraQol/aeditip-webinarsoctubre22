// Superclase Animal
public abstract class Animal {
	private String nombre;
	private int edad;
	private String colorPelo;
	private String raza;
	private int especie;
	
	public Animal(String nombre, int edad, String colorPelo,
			String raza) {
		this.nombre = nombre;
		this.edad = edad;
		this.colorPelo = colorPelo;
		this.raza = raza;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}
	
	public int getEspecie() {
		return especie;
	}

	public void setEspecie(int especie) {
		this.especie = especie;
	}
	
	public void setColorPelo(String colorPelo) {
		this.colorPelo = colorPelo;
	}
	
	public String getColorPelo() {
		return colorPelo;
	}
	
	public String toString() {
		return "Hola Mundo!";
	}
	
	public int calcularEdad() {
		return 0;
	}
}
