
public class Veterinaria {

	public static void main(String[] args) {	
		Animal[] animales = new Animal[6];
		animales[0] = new Perro("Pelusa", 7, "Caramelo", "Mestiza", 86);
		animales[1] = new Perro("Quilla", 6, "Blanco", "Mestiza", 88);
		animales[2] = new Perro("Perdida", 1, "Negro", "Mestiza", 90);
		animales[3] = new Perro("Clarita", 1, "Blanco", "Mestiza", 84);
		animales[4] = new Gato("Pelusa", 3, "Caramelo", "Desconocida", 1.25);
		animales[5] = new Caballo("Speedy", 2, "Marrón", "Desconocida", 41);
		
		Object obj;
		
		obj = animales[5];
		
		System.out.println(obj.toString());
	
		System.out.println("Edad: " + animales[5].calcularEdad());
	}

}
