import java.util.Scanner;

public class Ejercicio7{
    public static void main(String[] args){
		int opcion = 0;
        
		System.out.print("Bienvenido al programa. ");
		Scanner escaner = new Scanner(System.in);

		while(opcion != 9){
			System.out.println("Selecciona tu opción:");
			System.out.println("1) Suma dos números.");
			System.out.println("3) Calcula potenciación al cuadrado.");
			System.out.println("9) Sal del programa.");
			
			opcion = escaner.nextInt();
			
			switch(opcion){
				case 1:
					System.out.println("Ingrese los dos números (A y B).");
					int numA = escaner.nextInt();
					int numB = escaner.nextInt();
					System.out.println(numA + " + " + numB + " es " + (numA+numB) + ".");
					break;
				case 3:
					System.out.println("Ingrese el número.");
					int num = escaner.nextInt();
					System.out.println(num + " elevado al cuadrado es " + Math.pow(num, 2) + ".");	
					break;
				case 9:
					System.out.println("Gracias, que tenga un buen día.");
					break;
				default:
					System.out.println("Comando incorrecto. Intente de nuevo.");
					break;
				
			}

		}
		
		escaner.close();
    }
}