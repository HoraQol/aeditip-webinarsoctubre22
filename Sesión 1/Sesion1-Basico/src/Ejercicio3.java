import java.util.Scanner;

public class Ejercicio3{
	public static void main(String [] args){
		// Variable
		double varA = 2.5;
		// Constante
		final double varB = 2.5;
		// Modifación 1
		varA = 5;
		//varB = 6;
		final double varC;
		System.out.println("Ingrese dos valores");
		Scanner escaner = new Scanner(System.in);
		varA = escaner.nextDouble();
		varC = escaner.nextDouble();
		System.out.println("Impresión");
		System.out.println(varA);
		System.out.println(varB);
		System.out.println(varC);
		escaner.close();
	}
}