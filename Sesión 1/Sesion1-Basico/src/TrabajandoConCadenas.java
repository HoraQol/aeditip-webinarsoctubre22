public class TrabajandoConCadenas {
	public static void main(String[] args) {
		String ejemplo1 = "Hola";
		String ejemplo2 = "Mundo";
		System.out.println("Originales: " + ejemplo1 + " y " + ejemplo2);
		// Devuelve el caracter en la posición
		// Java cuenta desde cero (0)
		// El primero es 0, el segundo es 1, ...
		// Busco el cuarto caracter
		System.out.println("CharAt: " + ejemplo1.charAt(3));
		// Concatenar dos cadenas
		String combinado = ejemplo1.concat(" " + ejemplo2);
		System.out.println("Concat: " + 
				combinado);
		System.out.println("Suma: " + 
				ejemplo1.toUpperCase() + " " + ejemplo2.toLowerCase());
		// Compara si la cadena 1 es igual a la cadena 2
		System.out.println("Equals1: " +
				ejemplo1.equals(ejemplo2));
		String ejemplo3 = "Hola";
		System.out.println("Equals2: " +
				ejemplo1.equals(ejemplo3));
	}
}
