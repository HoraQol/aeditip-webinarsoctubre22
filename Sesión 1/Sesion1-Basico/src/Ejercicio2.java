import java.util.Scanner;

public class Ejercicio2{
	public static void main(String[] args){
		System.out.println("Por favor, escriba su nombre");
		// Permite leer la consola.
		Scanner escaner = new Scanner(System.in);
		String nombre = escaner.nextLine();
		System.out.println("Hola " + nombre + ". Bienvenido al Webinar Introducción a Java");
		escaner.close();
	}
}