
public class Arreglos {
	public static void main(String[] args){
		int estatico[] = {1, 2, 3};
		int tamano = 5;
		int dinamicoP[] = new int[5];
		int dinamicoI[] = new int[tamano];
		
		//System.out.println(estatico[1]);
		
		for(int i=0; i<5; i++) {
			dinamicoP[i] = i*2;
			dinamicoI[i] = i*2+1;
		}
		
		/*for(int i=0; i<5; i++) {
			System.out.println(dinamicoI[i]);
		}*/
		
		/*for(int elemento : dinamicoP) {
			System.out.println(elemento);
		}*/
		
		int dinamicoIAux[] = dinamicoI;
		tamano = 10;
		dinamicoI = new int[tamano];
		dinamicoI[5] = 11;
		
		for(int i=0; i<5; i++) {
			dinamicoI[i] = dinamicoIAux[i];
		}
			
		int i = 0;
		for(int elemento : dinamicoI) {
			System.out.println(elemento);
			i++;
		}
	}
}
