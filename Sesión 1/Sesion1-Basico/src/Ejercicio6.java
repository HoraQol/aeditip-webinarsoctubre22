import java.util.Scanner;

public class Ejercicio6{
    public static void main(String[] args){
		int mes;
        System.out.println("Ingresa el mes a consultar");
		Scanner escaner = new Scanner(System.in);
		mes = escaner.nextInt();
		
		switch(mes){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				System.out.println("El mes tiene 31 días.");
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				System.out.println("El mes tiene 30 días.");
				break;
			case 2:
				System.out.println("Escriba el año a consultar.");
				int anho = escaner.nextInt();
				if(anho % 4 == 0 && anho % 100 !=0)
					System.out.println("El año es bisiesto. Este mes tiene 29 días.");
				else
					System.out.println("El año no es bisiesto. Este mes tiene 28 días.");
				break;
			default:
				System.out.println("Cometió un error. El mes no es válido.");
				break;
		}
		
		escaner.close();
    }
}