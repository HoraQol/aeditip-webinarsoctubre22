public class SaludoPersonalizadoArgs{
	public static void main(String[] args){
		// Asumo que doy dos parámetros: nombre y país
		// args[0] <- Primer elemento (argumento)
		// args[1] <- Segundo elemento (argumento)
		System.out.println("Hola " + args[0] + " de " + args[1] + ". Bienvenido al Webinar Introducción a Java");
	}
}