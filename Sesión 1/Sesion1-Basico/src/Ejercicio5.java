import java.util.Scanner;

public class Ejercicio5{
    public static void main(String[] args){
		double temp;
		
        System.out.println("Ingresa la temperatura actual");
		Scanner escaner = new Scanner(System.in);
		temp = escaner.nextDouble();
		
		if(temp > 40){
			System.out.println("Caliente");
		// Operador conjuntivo: && (y)
		// Operador disyuntivo: || (o)
		}else if(temp >= 15 && temp <= 40){
			System.out.println("Templado");
		}else{
			System.out.println("Frío");
		}
		
		escaner.close();
    }
}