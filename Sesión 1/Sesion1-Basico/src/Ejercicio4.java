import java.util.Scanner;

public class Ejercicio4 {
	public static void main(String[] args){	
		final double PI = 3.141592;
		
		System.out.println("Ingrese el radio de la circunferencia");
	    Scanner escaner = new Scanner(System.in);
		double radio = escaner.nextDouble();
		
		double perimetro = 2 * PI * radio;
		double area = PI * radio * radio;
		
		System.out.println("Perímetro: " + perimetro);
		System.out.println("Área: " + area);
		
		escaner.close();
	}
}