package com.aeditip.veterinaria.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.veterinaria.entidades.Especie;

@Repository
public interface EspecieRepo extends JpaRepository<Especie, Integer> {
}
