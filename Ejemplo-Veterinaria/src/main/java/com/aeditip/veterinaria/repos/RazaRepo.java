package com.aeditip.veterinaria.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.veterinaria.entidades.Raza;

@Repository
public interface RazaRepo extends JpaRepository<Raza, Integer> {
}
