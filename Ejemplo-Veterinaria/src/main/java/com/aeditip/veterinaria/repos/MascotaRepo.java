package com.aeditip.veterinaria.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.veterinaria.entidades.Mascota;

@Repository
/* T: Clase que representa la tabla
 * ID: Clase que representa el identificador
 * 
 * Dimensión:
 * int <- Integer
 * double <- Double
 * String --
 * boolean <- Boolean
 */
public interface MascotaRepo extends JpaRepository<Mascota, Integer> {
	//@Query: WHERE ELIMINADO = 0
	//@Query: SELECT M.* FROM MASCOTA M, RAZA R, ESPECIE E
	//	WHERE R.ID_RAZA = M.ID_RAZA AND E.ID_ESPECIE = R.ID_ESPECIE AND 
	//		E.NOMBRE = 'Perro'
}
