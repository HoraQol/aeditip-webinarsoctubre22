package com.aeditip.veterinaria.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aeditip.veterinaria.entidades.Dueno;
import com.aeditip.veterinaria.entidades.Mascota;
import com.aeditip.veterinaria.entidades.Raza;
import com.aeditip.veterinaria.repos.DuenoRepo;
import com.aeditip.veterinaria.repos.MascotaRepo;
import com.aeditip.veterinaria.repos.RazaRepo;

@Service
public class MascotaServicio {
	@Autowired
	private MascotaRepo repo;
	@Autowired
	private DuenoRepo duenoRepo;
	@Autowired
	private RazaRepo razaRepo;
	
	// Crear (C): INSERT INTO mascota VALUES (....);
	public int crearMascota(Mascota mascota, int idDueno, int idRaza) {
		Dueno dueno = duenoRepo.findById(idDueno).orElse(null);
		Raza raza = razaRepo.findById(idRaza).orElse(null);
		mascota.setDueno(dueno);
		mascota.setRaza(raza);
		return repo.save(mascota).getId();
	}
	
	// Leer (R): SELECT * FROM mascota;
	public List<Mascota> obtenerTodasMascotas() {
		return repo.findAll();
	}
	
	// Leer (R): SELECT * FROM mascota WHERE id_mascota = '?';
	public Mascota obtenerUnaMascota(int id) {
		return repo.findById(id).orElse(null);
	}
	
	// Modificar (U): UPDATE FROM mascota SET (...) WHERE id_mascota = '?';
	public Mascota editarMascota(Mascota mascota, int id) {
		if(repo.findById(id).isPresent())
			return repo.save(mascota);
		else
			return null;
	}
	
	// Eliminar físicamente (D): DELETE FROM mascota WHERE ...
	public int retirarMascota(int id) {
		if(repo.findById(id).isPresent()) {
			repo.deleteById(id);
			return 1;
		}else return 0;
	}
}
