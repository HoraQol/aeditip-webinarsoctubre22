package com.aeditip.veterinaria.controladores;

import java.util.List;

/* Conexión entre los servicios web y el usuario */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.veterinaria.entidades.Mascota;
import com.aeditip.veterinaria.servicios.MascotaServicio;

@RestController
// http://localhost:8081/api/mascotas
@RequestMapping("/api/mascotas")
public class MascotaControlador {
	@Autowired
	private MascotaServicio servicio;
	
	// POST http://localhost:8081/api/mascotas
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String crearMascota(@RequestBody Mascota mascota, @RequestParam int idDueno, @RequestParam int idRaza) {
		return "Mascota registrada con el ID " + servicio.crearMascota(mascota, idDueno, idRaza);
	}
	
	// GET http://localhost:8081/api/mascotas
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<Mascota> obtenerTodasMascotas() {
		return servicio.obtenerTodasMascotas();
	}
	
	// GET http://localhost:8081/api/mascotas
	// Leer (R): SELECT * FROM mascota WHERE id_mascota = '?';
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Mascota obtenerUnaMascota(@PathVariable int id) {
		Mascota mascota = servicio.obtenerUnaMascota(id);
		if(mascota == null) {
			System.out.println("La mascota no existe");
			return null;
		}else 
			return mascota;
	}
	
	// PUT http://localhost:8081/api/mascotas
	// Modificar (U): UPDATE FROM mascota SET (...) WHERE id_mascota = '?';
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public Mascota editarMascota(@RequestBody Mascota mascota, @PathVariable int id) {
		Mascota mascotaOriginal = servicio.editarMascota(mascota, id);
		if(mascotaOriginal == null) {
			System.out.println("La mascota no existe");
			return null;
		}else 
			return mascotaOriginal;
	}
	
	// DELETE http://localhost:8081/api/mascotas
	// Eliminar físicamente (D): DELETE FROM mascota WHERE ...
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public int retirarMascota(@PathVariable int id) {
		int estado = servicio.retirarMascota(id);
		if(estado == 0) {
			System.out.println("La mascota no existe");
			return 0;
		}else 
			return 1;
	}
}
