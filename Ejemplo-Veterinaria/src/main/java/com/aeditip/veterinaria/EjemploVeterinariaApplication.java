package com.aeditip.veterinaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjemploVeterinariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjemploVeterinariaApplication.class, args);
	}

}
